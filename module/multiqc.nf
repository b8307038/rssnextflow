
process multiqc {
    publishDir "${params.outdir}", mode: 'copy'

    executor = 'local'

    //beforeScript 'source /home/wyl37/mypython3base/bin/activate'

    input:
    path fastqc
    path kallistoquant
    path staraligned
    path subreadCount
    path picard
    path jemarkdup
    path biotypePercentages
    path multiqc_configfolder
    path bam_stat
    path inner_dist_freq
    path infer_experiment
    path junction_annotation
    path junction_saturation
    path read_distribution
    path read_duplication
    path preseq

    output:
    path 'multiqc_report.html'
    path 'multiqc_data', emit: multiqcdata

    script:
    """
    cp $multiqc_configfolder/* .

    echo "custom_logo: \$PWD/clusterlogo.png" >> multiqc_config.yaml

    multiqc -v .
    """
}

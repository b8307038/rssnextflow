process biotypePercentages {
    label 'process_1low'
    executor = "local"
    publishDir "${params.outdir}/biotypePercentages"

    input:
    path fc_readCounts

    output:
    path "biotype_counts_mqc.tsv", emit: biotype_percentages

    script:
    """
    #! /usr/bin/env python3
    with open("fc_readCounts.txt") as f:
        f.readline()
        sample_names = f.readline().split()[8:]
        biotype_dict = {}
        
        for line in f:
            splat = line.split()
            biotype_name = splat[7]
            counts = [int(x) for x in splat[8:]]
            if biotype_name not in biotype_dict.keys():
                biotype_dict[biotype_name] = counts
            else:
                biotype_dict[biotype_name] = \
                    [x+y for x,y in zip(biotype_dict[biotype_name], counts)]
                        
    biotype_counts_dict = {}
    for sample_num,sample_name in enumerate(sample_names):
        biotype_counts_dict[sample_name] = [x[sample_num] for x in biotype_dict.values()]
        
    with open("biotype_counts_mqc.tsv", "w") as f:
        print("sample_name" + "\t" + "\t".join(biotype_dict.keys()), file = f)
        for k,v in biotype_counts_dict.items():
            print(k.split(".")[0] + "\t" + "\t".join([str(x) for x in v]), file = f)
    """
}


process preseq {
    label 'process_1low'
    label 'error_ignore'
    publishDir "${params.outdir}/preseq"

    input:
    path bamfile

    output:
    path("*.ccurve.txt"), emit: preseq_ccurve

    script:
    sampleId = "${bamfile.simpleName}"
    """
    # Setting insert size to 10000000 to avoid crashing, where the crash is
    # blamed on the BAM file being unsorted
    # Using fast mode (-Q)
    preseq lc_extrap -pe -output ${sampleId}.preseq_out.ccurve.txt -l 10000000 -Q -bam ${bamfile}
    """
}

// fastQC
process fastqc {
    tag "$sampleId"
    label 'process_low'
    publishDir "${params.outdir}/fastqc"

    input:
    tuple val(sampleId), path(reads)

    output:
    file '*_fastqc.{zip,html}'

    script:

    """
    zcat ${reads[0]} | fastqc stdin:"${sampleId}_R1" -t ${task.cpus}
    zcat ${reads[1]} | fastqc stdin:"${sampleId}_R3" -t ${task.cpus}

    """
}


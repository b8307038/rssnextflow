process rseqc_junctionSaturation {
    tag "$sampleId"
    label 'process_1low'
    publishDir "${params.outdir}/rseqc_junctionSaturation"

    input:
    path bamfile
    path bedfile

    output:
    path("*.r"), emit: r_script

    script:
    sampleId = "${bamfile.simpleName}"
    """
        junction_saturation.py -i ${bamfile} -r ${bedfile} -o ${sampleId}
    """
}

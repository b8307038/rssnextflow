
process genomeref {
    publishDir 'refgenomes'

    executor = 'local'

    output:
    path 'Homo_sapiens.GRCh38.cdna.all.fa.gz', emit: cdnaref
    path 'Homo_sapiens.GRCh38.103.gtf.gz' , emit: gtfref
    path 'Homo_sapiens.GRCh38.103.gff3.gz' , emit: gffref
    path 'Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz', emit: dnaref

    script:
    '''
    wget http://ftp.ensembl.org/pub/release-103/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
    wget http://ftp.ensembl.org/pub/release-103/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz
    wget http://ftp.ensembl.org/pub/release-103/gtf/homo_sapiens/Homo_sapiens.GRCh38.103.gtf.gz
    wget http://ftp.ensembl.org/pub/release-103/gff3/homo_sapiens/Homo_sapiens.GRCh38.103.gff3.gz
    '''
}

process rseqc_bamstat {
    tag "$sampleId"
    label 'process_1low'
    publishDir "${params.outdir}/rseqc_bamstat"

    input:
    path bamfile

    output:
    path("*.bam_stat.txt"), emit: bam_stat

    script:
    sampleId = "${bamfile.simpleName}"
    """
    bam_stat.py -i ${bamfile} > ${sampleId}.bam_stat.txt
    """
}

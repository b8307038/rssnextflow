process dupRadar {
    label 'process_8himem'
    publishDir "${params.outdir}/dupRadar"

    input:
    path bamfiles
    path gtfref

    output:
    path("*.pdf")    , emit: dupradar_pdf
    path("*.txt")    , emit: dupradar_txt
    path("*_mqc.txt"), emit: dupradar_multiqc

    script:
    // Is it correct to say that it is unstranded (the 0 parameter)?
    // Need to add dupradar to the singularity image
    """
    #zcat $gtfref > gtf.out
    /lustre/home/sejjgh3/bin/dupradar.r $bamfiles dupradar_out $gtfref 0 paired 8
    """
}

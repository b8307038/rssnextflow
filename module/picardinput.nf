
process picardinput {
    publishDir "${projectDir}/refgenomes"

    executor = 'local'

    input:
    path dnaref
    path gtfref

    output:
    path 'GRCh38.103.rRNA.interval_list', emit:rRNA_list
    path 'GRCh38.103.refFlat', emit: refFlat
    path 'GRCh38.103.sizes.genome', emit: sizesgenome

    script:
    rRNA = 'GRCh38.103.rRNA.interval_list'

    """
    zcat ${dnaref} > ${dnaref.baseName}
    samtools faidx ${dnaref.baseName}
    cut -f1,2 "${dnaref.baseName}.fai" > GRCh38.103.sizes.genome

    zcat ${gtfref} > ${gtfref.baseName}

    make_rRNA.sh "GRCh38.103.sizes.genome" "${gtfref.baseName}" "${rRNA}"

    java -jar \$PICARD CreateSequenceDictionary \\
    REFERENCE=${dnaref.baseName} \\
    OUTPUT="${dnaref.baseName}.dict"

    ConvertToRefFlat \\
    ANNOTATIONS_FILE="${gtfref.baseName}" \\
    SEQUENCE_DICTIONARY="${dnaref.baseName}.dict" \\
    OUTPUT=GRCh38.103.refFlat

    rm -f ${dnaref.baseName} ${gtfref.baseName}

    """
}

process picardRNAQC {
    tag "${bam.simpleName}"
    label 'process_1low'
    publishDir "${params.outdir}/PICARD"

    input:
    path bam
    path refFlat
    path rRNA_list

    output:
    path "${outres}"

    script:
    maxreads = 2500000 * task.attempt
    memgb = 6 * task.attempt
    if ("${bam.name}" =~ /pseudoalignments/) {
        outres = "${bam.simpleName}.kallisto.RNA_Metrics"
    } else {
        outres = "${bam.simpleName}.RNA_Metrics"
    }

    """
    java -Xmx${memgb}G -jar \$PICARD CollectRnaSeqMetrics \\
    I=${bam} O=$outres \\
    REF_FLAT=${refFlat} STRAND=FIRST_READ_TRANSCRIPTION_STRAND \\
    RIBOSOMAL_INTERVALS=${rRNA_list} MAX_RECORDS_IN_RAM=$maxreads

    """
}

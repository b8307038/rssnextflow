process rseqc_innerDistance {
    tag "$sampleId"
    label 'process_1low'
    publishDir "${params.outdir}/rseqc_innerDistance"

    input:
    path bamfile
    path bedfile

    output:
    path("*freq.txt"), emit: inner_dist_freq

    script:
    sampleId = "${bamfile.simpleName}"
    """
        inner_distance.py -i ${bamfile} -r ${bedfile} -o ${sampleId} > stdout.txt
        head -n 2 stdout.txt > ${sampleId}.inner_distance_mean_mqc.txt
    """
}

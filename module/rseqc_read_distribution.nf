process rseqc_readDistribution {
    tag "$sampleId"
    label 'process_1low'
    publishDir "${params.outdir}/rseqc_readDistribution"

    input:
    path bamfile
    path bedfile

    output:
    path("*.txt"), emit: read_distribution_txt

    script:
    sampleId = "${bamfile.simpleName}"
    """
        read_distribution.py -i ${bamfile} -r ${bedfile} > ${sampleId}.read_distribution.txt
    """
}

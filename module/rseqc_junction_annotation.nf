process rseqc_junctionAnnotation {
    tag "$sampleId"
    label 'process_1low'
    publishDir "${params.outdir}/rseqc_junctionAnnotation"

    input:
    path bamfile
    path bedfile

    output:
    path("*.log"), emit: log

    script:
    sampleId = "${bamfile.simpleName}"
    """
        junction_annotation.py -i ${bamfile} -r ${bedfile} -o ${sampleId} 2> ${sampleId}.junction_annotation.log
    """
}

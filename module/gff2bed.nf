process gff2bed {
    label 'process_1low'
    publishDir "${params.outdir}/gff2bed"

    input:
    path gffref

    output:
    path("*.bed"), emit: ref_bed

    script:
    """
    zcat ${gffref} > Homo_sapiens.GRCh38.103.gff3
    gff3ToGenePred Homo_sapiens.GRCh38.103.gff3 out.gp
    genePredToBed out.gp Homo_sapiens.GRCh38.103.bed
    """
}

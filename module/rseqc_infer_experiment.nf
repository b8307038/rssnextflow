process rseqc_inferExperiment {
    tag "$sampleId"
    label 'process_1low'
    publishDir "${params.outdir}/rseqc_infer_experiment"

    input:
    path bamfile
    path bedfile

    output:
    path("*infer_experiment.txt"), emit: infer_experiment

    script:
    sampleId = "${bamfile.simpleName}"
    """
        infer_experiment.py -i ${bamfile} -r ${bedfile} > ${sampleId}.infer_experiment.txt
    """
}

process rseqc_readDuplication {
    tag "$sampleId"
    label 'process_himem'
    publishDir "${params.outdir}/rseqc_readDuplication"

    input:
    path bamfile

    output:
    path("*pos.DupRate.xls"), emit: position_duplication_xls

    script:
    sampleId = "${bamfile.simpleName}"
    """
        read_duplication.py -i ${bamfile} -o ${sampleId}
    """
}

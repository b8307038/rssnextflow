#! /usr/bin/env nextflow

nextflow.enable.dsl = 2

include { jedupnew } from './module/jesuite'

include { fastqc } from './module/fastqc'

include { genomeref } from './module/genomeprep'

include { gff2bed } from './module/gff2bed'

include { kallistoindex; kallistoquant } from './module/kallisto'

include { staridx; staraligned } from './module/starprocess'

include { subreadCount; subreadCountgeneName } from './module/subread.nf'

include { picardinput; picardRNAQC } from './module/picardinput'

//include { dupRadar } from './module/dupradar'

include { biotypePercentages } from './module/biotype_percentages'

include { preseq } from './module/preseq'

include { rseqc_bamstat } from './module/rseqc_bamstat'
include { rseqc_inferExperiment } from './module/rseqc_infer_experiment'
include { rseqc_innerDistance } from './module/rseqc_inner_distance'
include { rseqc_junctionAnnotation } from './module/rseqc_junction_annotation'
include { rseqc_junctionSaturation } from './module/rseqc_junction_saturation'
include { rseqc_readDistribution } from './module/rseqc_read_distribution'
include { rseqc_readDuplication } from './module/rseqc_read_duplication'

include { multiqc } from './module/multiqc.nf'

/////////////////////////////////////
// channel for multqc_configfolder //
/////////////////////////////////////
multiqc_configfolder_chl = Channel.fromPath(params.multiqc_configfolder, checkIfExists: true)

////////////////////////////
// channel for read_pairs //
////////////////////////////

if (params.sampleCSV) {
    sampleFile = file(params.sampleCSV, checkIfExists: true)
} else {
    exit 1, 'samplecsv (--sampleCSV) is required!'
}

Channel
    .fromPath( sampleFile )
    .splitCsv( header:true )
    .map{ row -> tuple( row.sampleId, [ file( row.read1, checkIfExists: true ), file( row.read2, checkIfExists: true )] )}
    .take( params.dev ? params.number_of_inputs : -1)
    .set { read_pairs_ch }


workflow {
    genomeref()

    gff2bed(genomeref.out.gffref)

    picardinput(genomeref.out.dnaref, genomeref.out.gtfref)

    fastqc(read_pairs_ch)

    staridx(genomeref.out.dnaref, genomeref.out.gtfref)

    staraligned(read_pairs_ch, staridx.out.starindex, genomeref.out.gtfref)

    jedupnew(staraligned.out.bam_sorted)

    kallistoindex(genomeref.out.cdnaref)

    kallistoquant(jedupnew.out.dedupfq, kallistoindex.out,
               genomeref.out.gtfref,
               picardinput.out.sizesgenome)

    subreadCount(jedupnew.out.markdupbam.collect(),
		 genomeref.out.gtfref)
    
    subreadCountgeneName(jedupnew.out.markdupbam.collect(),
			 genomeref.out.gtfref)

    picardRNAQC(jedupnew.out.markdupbam,
             picardinput.out.refFlat,
             picardinput.out.rRNA_list
    )

    preseq(staraligned.out.bam_sorted)

    rseqc_bamstat(staraligned.out.bam_sorted)
    rseqc_inferExperiment(staraligned.out.bam_sorted, gff2bed.out.ref_bed)
    rseqc_innerDistance(staraligned.out.bam_sorted, gff2bed.out.ref_bed)
    rseqc_junctionAnnotation(staraligned.out.bam_sorted, gff2bed.out.ref_bed)
    rseqc_junctionSaturation(staraligned.out.bam_sorted, gff2bed.out.ref_bed)
    rseqc_readDistribution(staraligned.out.bam_sorted, gff2bed.out.ref_bed)
    rseqc_readDuplication(staraligned.out.bam_sorted)

    // dupRadar(staraligned.out.bam_sorted, genomeref.out.gtfref)

    biotypePercentages(subreadCount.out.counts)

    multiqc(fastqc.out.collect(),
             kallistoquant.out.kalrun_log.collect(),
             staraligned.out.log_final.collect(),
             subreadCount.out.fcsummary,
            picardRNAQC.out.collect(),
            jedupnew.out.markdupmetrics.collect(),
            biotypePercentages.out.biotype_percentages.collect(),
            multiqc_configfolder_chl,
            rseqc_bamstat.out.bam_stat.collect(),
            rseqc_innerDistance.out.inner_dist_freq.collect(),
            rseqc_inferExperiment.out.infer_experiment.collect(),
            rseqc_junctionAnnotation.out.log.collect(),
            rseqc_junctionSaturation.out.r_script.collect(),
            rseqc_readDistribution.out.read_distribution_txt.collect(),
            rseqc_readDuplication.out.position_duplication_xls.collect(),
            preseq.out.preseq_ccurve.collect())
}

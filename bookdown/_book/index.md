--- 
title: "bulk RNAseq with UMI"
author: "Wei-Yu"
date: "2022-05-19"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
---

# Introduction

- UMI (**U**nique **M**olecular **I**dentifier) is used in bulk RNAseq for PBMC, CD4, CD8, CD14, and CD19.  
  - UMI of 5 nucleotides: Batch 1  
  - UMI of 8 nucleotides: Batch 2

- Marking duplicate reads (or de-duplication) can't not be done at raw fastq level because  
  - $4^5$/$4^8$ UMIs tagged to million reads  
  - exact read sequences and UMI do not guarantee PCR duplicates
  - sequencing errors in both UMI and reads

- Marking duplicate reads (or de-duplication) is performed following STAR alignment step.
  - allowing mismatch of 1 for UMI

- Workflow are amened to account for PCR duplicates identified using UMI.

- FASTQ generation software changes from bcl2fastq to BCL Convert 
  - Main difference is that BCL Convert integrates UMI to the read header and thus only 2 fastq.gz will be generated per sample, compared to 3 fastq.gz (R2 is UMI) from bcl2fastq.
  - ~~Currently, Batch 1 fastq files were generated using bcl2fastq and will be generated again using BCL Convert.~~
  - Batch 1 fastq files were generated again using BCL Convert.
  - BCL Convert is used for Batch 2 and later batches.



<div class="figure">
<img src="./flowchart_dag.png" alt="RNAseq (BCL Convert) processing workflow" width="100%" />
<p class="caption">(\#fig:workflow)RNAseq (BCL Convert) processing workflow</p>
</div>


 

